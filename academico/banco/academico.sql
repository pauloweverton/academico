-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 20-Nov-2016 às 21:09
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `academico`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_aluno`
--

CREATE TABLE `tb_aluno` (
  `id` int(11) NOT NULL,
  `matricula` varchar(50) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `data_ingresso` date DEFAULT NULL,
  `tb_curso_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_aluno_disciplina`
--

CREATE TABLE `tb_aluno_disciplina` (
  `id` int(11) NOT NULL,
  `tb_periodo_letivo_id` int(11) NOT NULL,
  `tb_aluno_id` int(11) NOT NULL,
  `tb_disciplina_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_curso`
--

CREATE TABLE `tb_curso` (
  `id` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `tb_tipo_curso_id` int(11) NOT NULL,
  `tb_nivel_curso_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_curso`
--

INSERT INTO `tb_curso` (`id`, `codigo`, `nome`, `tb_tipo_curso_id`, `tb_nivel_curso_id`) VALUES
(1, 'SIS1', 'Sistemas de Informação', 2, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_disciplina`
--

CREATE TABLE `tb_disciplina` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `tb_curso_id` int(11) NOT NULL,
  `tb_turno_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_nivel_curso`
--

CREATE TABLE `tb_nivel_curso` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_nivel_curso`
--

INSERT INTO `tb_nivel_curso` (`id`, `nome`) VALUES
(1, 'Médio'),
(2, 'Técnico'),
(3, 'Bacharelado'),
(4, 'Especialização'),
(5, 'Mestrado'),
(6, 'Doutorado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_periodo_letivo`
--

CREATE TABLE `tb_periodo_letivo` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_tipo_curso`
--

CREATE TABLE `tb_tipo_curso` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_tipo_curso`
--

INSERT INTO `tb_tipo_curso` (`id`, `nome`) VALUES
(1, 'A Distância'),
(2, 'Presencial');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_turno`
--

CREATE TABLE `tb_turno` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_aluno`
--
ALTER TABLE `tb_aluno`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `matricula` (`matricula`),
  ADD UNIQUE KEY `cpf` (`cpf`),
  ADD KEY `fk_curso_id_idx` (`tb_curso_id`);

--
-- Indexes for table `tb_aluno_disciplina`
--
ALTER TABLE `tb_aluno_disciplina`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_periodo_letivo_id` (`tb_periodo_letivo_id`),
  ADD KEY `tb_aluno_id` (`tb_aluno_id`),
  ADD KEY `tb_disciplina_id` (`tb_disciplina_id`);

--
-- Indexes for table `tb_curso`
--
ALTER TABLE `tb_curso`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `fk_nivel_curso_id` (`tb_nivel_curso_id`),
  ADD KEY `fk_tipo_curso_id` (`tb_tipo_curso_id`);

--
-- Indexes for table `tb_disciplina`
--
ALTER TABLE `tb_disciplina`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tb_curso_id` (`tb_curso_id`),
  ADD KEY `tb_turno_id` (`tb_turno_id`);

--
-- Indexes for table `tb_nivel_curso`
--
ALTER TABLE `tb_nivel_curso`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_periodo_letivo`
--
ALTER TABLE `tb_periodo_letivo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tipo_curso`
--
ALTER TABLE `tb_tipo_curso`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_turno`
--
ALTER TABLE `tb_turno`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_aluno`
--
ALTER TABLE `tb_aluno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_aluno_disciplina`
--
ALTER TABLE `tb_aluno_disciplina`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_curso`
--
ALTER TABLE `tb_curso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_disciplina`
--
ALTER TABLE `tb_disciplina`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_nivel_curso`
--
ALTER TABLE `tb_nivel_curso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_periodo_letivo`
--
ALTER TABLE `tb_periodo_letivo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_tipo_curso`
--
ALTER TABLE `tb_tipo_curso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_turno`
--
ALTER TABLE `tb_turno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_aluno`
--
ALTER TABLE `tb_aluno`
  ADD CONSTRAINT `fk_curso_id` FOREIGN KEY (`tb_curso_id`) REFERENCES `tb_curso` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_aluno_disciplina`
--
ALTER TABLE `tb_aluno_disciplina`
  ADD CONSTRAINT `tb_aluno_disciplina_ibfk_1` FOREIGN KEY (`tb_periodo_letivo_id`) REFERENCES `tb_periodo_letivo` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_aluno_disciplina_ibfk_2` FOREIGN KEY (`tb_aluno_id`) REFERENCES `tb_aluno` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_aluno_disciplina_ibfk_3` FOREIGN KEY (`tb_disciplina_id`) REFERENCES `tb_disciplina` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_curso`
--
ALTER TABLE `tb_curso`
  ADD CONSTRAINT `fk_nivel_curso_id` FOREIGN KEY (`tb_nivel_curso_id`) REFERENCES `tb_nivel_curso` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tipo_curso_id` FOREIGN KEY (`tb_tipo_curso_id`) REFERENCES `tb_tipo_curso` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Limitadores para a tabela `tb_disciplina`
--
ALTER TABLE `tb_disciplina`
  ADD CONSTRAINT `tb_disciplina_ibfk_1` FOREIGN KEY (`tb_curso_id`) REFERENCES `tb_curso` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_disciplina_ibfk_2` FOREIGN KEY (`tb_turno_id`) REFERENCES `tb_turno` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
