package br.edu.uepb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import br.edu.uepb.entidades.EntidadeBase;

public class DAO<T extends EntidadeBase> {
	
	private ConnectionFactory connFactory = ConnectionFactory.getInstance();
	private Class<T> clazz;
	
	public DAO(Class<T> clazz) {
		this.clazz = clazz;
	}
	
	public T salvar(T obj) {
		EntityManager em = this.connFactory.getEM();
		try {
			em.getTransaction().begin();
			
			if (obj.getId() == null) {
				em.persist(obj);
			} else {
				if (!em.contains(obj)) {
					if (em.find(this.clazz, obj).getId() == null) {
						throw new Exception("Objeto n�o mais dispon�vel para atualiza��o");
					}
				}
				em.merge(obj);
			}
			em.getTransaction().commit();
			return obj;
			
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			
		} finally {
			em.close();
		}
		return null;
	}
	
	public T ler(Long id) {
		EntityManager em = this.connFactory.getEM();
		try {
			T obj = em.find(this.clazz, id);
			return obj;
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			em.close();
		}
		return null;
	}
	
	public List<T> lerTudo() {
		try {
			CriteriaQuery<T> query = connFactory.getEM().getCriteriaBuilder().createQuery(this.clazz);
			query.select(query.from(this.clazz));
			
			return connFactory.getEM().createQuery(query).getResultList();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Boolean remover(Long id) {
		EntityManager em = this.connFactory.getEM();
		try {
			T obj = em.find(this.clazz, id);
			if (obj == null) {
				throw new Exception("O registro n�o est� mais dispon�vel para remo��o");
			}
			em.getTransaction().begin();
			em.remove(obj);
			em.getTransaction().commit();
			
			return true; 
			
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			em.close();
		}
		return false;
	}
}
