package br.edu.uepb.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ConnectionFactory {
	
	private static ConnectionFactory connectionFactory;
	private EntityManagerFactory entityManagerFactory;
	
	private ConnectionFactory() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("academico");
	}
	
	public static ConnectionFactory getInstance() {
		if (connectionFactory == null) {
			connectionFactory = new ConnectionFactory();
		}
		return connectionFactory;
	}
	
	public EntityManager getEM() {
		if (entityManagerFactory != null) {
			return entityManagerFactory.createEntityManager();
		}
		return null;
	}
}