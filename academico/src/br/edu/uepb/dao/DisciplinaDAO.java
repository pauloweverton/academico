package br.edu.uepb.dao;

import br.edu.uepb.entidades.Disciplina;

public class DisciplinaDAO extends DAO<Disciplina> {

	public DisciplinaDAO() {
		super(Disciplina.class);
	}
}
