package br.edu.uepb.dao;

import br.edu.uepb.entidades.Curso;

public class CursoDAO extends DAO<Curso> {

	public CursoDAO() {
		super(Curso.class);
	}
}
