package br.edu.uepb.dao;

import br.edu.uepb.entidades.Aluno;

public class AlunoDAO extends DAO<Aluno> {

	public AlunoDAO() {
		super(Aluno.class);
	}

}