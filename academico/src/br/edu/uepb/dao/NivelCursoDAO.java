package br.edu.uepb.dao;

import br.edu.uepb.entidades.NivelCurso;

public class NivelCursoDAO extends DAO<NivelCurso>{

	public NivelCursoDAO() {
		super(NivelCurso.class);
	}
}
