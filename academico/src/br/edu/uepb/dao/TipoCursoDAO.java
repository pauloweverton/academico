package br.edu.uepb.dao;

import br.edu.uepb.entidades.TipoCurso;

public class TipoCursoDAO extends DAO<TipoCurso>{

	public TipoCursoDAO() {
		super(TipoCurso.class);
	}
}
