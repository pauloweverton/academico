package br.edu.uepb.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_curso")
public class Curso implements EntidadeBase, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false, unique=true, length=10)
	private String codigo;
	
	@Column(nullable=false, length=100)
	private String nome;
	
	@ManyToOne
	@JoinColumn(name="tb_tipo_curso_id")
	private TipoCurso tipo;
	
	@ManyToOne
	@JoinColumn(name="tb_nivel_curso_id")
	private NivelCurso nivel;
	
	public Curso() {}
	
	public Curso(Long id) {
		this.id = id;
	}
	
	public Curso(String codigo, String nome, TipoCurso tipo, NivelCurso nivel) {
		this.codigo = codigo;
		this.nome = nome;
		this.tipo = tipo;
		this.nivel = nivel;
	}
	
	public Curso(Long id, String codigo, String nome, TipoCurso tipo, NivelCurso nivel) {
		this.id = id;
		this.codigo = codigo;
		this.nome = nome;
		this.tipo = tipo;
		this.nivel = nivel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoCurso getTipo() {
		return tipo;
	}

	public void setTipo(TipoCurso tipo) {
		this.tipo = tipo;
	}

	public NivelCurso getNivel() {
		return nivel;
	}

	public void setNivel(NivelCurso nivel) {
		this.nivel = nivel;
	}
	
	@Override
	public int hashCode() {
		return 5 * id.intValue();
	}
	
	@Override
	public boolean equals(Object o) {
		if ((o != null) && (o instanceof Curso)) {
			Curso c = (Curso) o;
			if (c.getId() == this.id) {
				return true;
			}
		}
		return false;
	}
}
