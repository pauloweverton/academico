package br.edu.uepb.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_periodo_letivo")
public class PeriodoLetivo implements EntidadeBase {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false, length=50)
	private String nome;

	public PeriodoLetivo() {}
	
	public PeriodoLetivo(Long id) {
		this.id = id;
	}
	
	public PeriodoLetivo(String nome) {
		this.nome = nome;
	}
	
	public PeriodoLetivo(Long id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
