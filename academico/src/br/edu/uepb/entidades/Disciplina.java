package br.edu.uepb.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_disciplina")
public class Disciplina implements EntidadeBase {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false, length=100)
	private String nome;
	
	@ManyToOne
	@JoinColumn(name="tb_curso_id")
	private Curso curso;
	
	@ManyToOne
	@JoinColumn(name="tb_turno_id")
	private Turno turno;

	public Disciplina() {}
	
	public Disciplina(Long id) {
		this.id = id;
	}
	
	public Disciplina(String nome, Curso curso, Turno turno) {
		this.nome = nome;
		this.curso = curso;
		this.turno = turno;
	}
	
	public Disciplina(Long id, String nome, Curso curso, Turno turno) {
		this.id = id;
		this.nome = nome;
		this.curso = curso;
		this.turno = turno;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Turno getTurno() {
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}
}
