package br.edu.uepb.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_tipo_curso")
public class TipoCurso implements EntidadeBase, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false, length=100)
	private String nome;
	
	public TipoCurso() {}

	public TipoCurso(Long id) {
		this.id = id;
	}

	public TipoCurso(Long id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		return 8 * id.intValue();
	}
	
	@Override
	public boolean equals(Object o) {
		if ((o != null) && (o instanceof TipoCurso)) {
			TipoCurso t = (TipoCurso) o;
			
			if (t.getId() == this.id) {
				return true;
			}
		}
		return false;
	}
}
