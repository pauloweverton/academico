package br.edu.uepb.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_nivel_curso")
public class NivelCurso implements EntidadeBase, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false, length=100)
	private String nome;

	public NivelCurso() {}

	public NivelCurso(Long id) {
		this.id = id;
	}

	public NivelCurso(String nome) {
		this.nome = nome;
	}
	
	public NivelCurso(Long id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		return 10 * id.intValue();
	}
	
	public boolean equals(Object o) {
		if (o != null && (o instanceof NivelCurso)) {
			NivelCurso nc = (NivelCurso) o;
			if (nc.getId() == this.id) {
				return true;
			}
		}
		return false;
	}
}
