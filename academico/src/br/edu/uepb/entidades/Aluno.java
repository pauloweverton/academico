package br.edu.uepb.entidades;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="tb_aluno")
public class Aluno implements EntidadeBase {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false, unique=true, length=50)
	private String matricula;
	
	@Column(nullable=false, unique=true, length=11)
	private String cpf;
	
	@Column(nullable=false, length=255)
	private String nome;
	
	@Column(name="data_ingresso")
	@Temporal(TemporalType.DATE)
	private Date dataIngresso;
	
	@ManyToOne
	@JoinColumn(name="tb_curso_id")
	private Curso curso;
	
	@ManyToMany
	@JoinTable(name="aluno_disciplina", joinColumns = @JoinColumn(name="tb_aluno_id"), inverseJoinColumns = @JoinColumn(name="tb_disciplina_id"))
	private List<Disciplina> disciplinas;

	public Aluno() {}
	
	public Aluno(Long id) {
		this.id = id;
	}

	public Aluno(String matricula, String cpf, String nome, Date dataIngresso) {
		this.matricula = matricula;
		this.cpf = cpf;
		this.nome = nome;
		this.dataIngresso = dataIngresso;
	}
	
	public Aluno(Long id, String matricula, String cpf, String nome, Date dataIngresso) {
		this.id = id;
		this.matricula = matricula;
		this.cpf = cpf;
		this.nome = nome;
		this.dataIngresso = dataIngresso;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataIngresso() {
		return dataIngresso;
	}

	public void setDataIngresso(Date dataIngresso) {
		this.dataIngresso = dataIngresso;
	}
}
