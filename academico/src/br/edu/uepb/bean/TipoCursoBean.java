package br.edu.uepb.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.edu.uepb.dao.TipoCursoDAO;
import br.edu.uepb.entidades.TipoCurso;

@ManagedBean
@RequestScoped
public class TipoCursoBean {

	private List<TipoCurso> tiposCurso;
	
	public TipoCursoBean() {
		this.tiposCurso = new TipoCursoDAO().lerTudo();
	}
	
	public List<TipoCurso> getTiposCurso() {
		return tiposCurso;
	}
}
