package br.edu.uepb.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.edu.uepb.dao.CursoDAO;
import br.edu.uepb.entidades.Curso;
import br.edu.uepb.entidades.NivelCurso;
import br.edu.uepb.entidades.TipoCurso;

@ManagedBean
@RequestScoped
public class CursoBean {

	private Curso curso; // Objeto auxiliar para opera��es do CRUD
	private List<Curso> cursos; // Objeto para realizar a listagem dos cursos.
	
	private Long idTipoCursoSelecionado;
	private Long idNivelCursoSelecionado;
	
	public CursoBean() {
		this.curso = new Curso();
		this.cursos = new CursoDAO().lerTudo();
	}
	
	public String salvar() {
		TipoCurso tTemp = new TipoCurso(idTipoCursoSelecionado);
		NivelCurso nTemp = new NivelCurso(idNivelCursoSelecionado);
		curso.setTipo(tTemp);
		curso.setNivel(nTemp);
		new CursoDAO().salvar(curso);
		this.cursos = new CursoDAO().lerTudo();
		
		return "cursos";
	}
	
	public String prepararEditar(Curso curso) {
		this.curso = curso;
		this.idNivelCursoSelecionado = curso.getNivel().getId();
		this.idTipoCursoSelecionado = curso.getTipo().getId();
		
		return "editar_curso";
	}
	
	public String remover(Long idCurso) {
		new CursoDAO().remover(idCurso);
		this.cursos = new CursoDAO().lerTudo();
		
		return "cursos";
	}
	
	public String prepararEdicao(Curso c) {
		this.curso = c;
		return "manter_curso";
	}
	
	public Curso getCurso() {
		return curso;
	}
	
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	
	public List<Curso> getCursos() {
		return cursos;
	}
	
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	
	public Long getIdTipoCursoSelecionado() {
		return idTipoCursoSelecionado;
	}
	
	public void setIdTipoCursoSelecionado(Long idTipoCursoSelecionado) {
		this.idTipoCursoSelecionado = idTipoCursoSelecionado;
	}
	
	public Long getIdNivelCursoSelecionado() {
		return idNivelCursoSelecionado;
	}
	
	public void setIdNivelCursoSelecionado(Long idNivelCursoSelecionado) {
		this.idNivelCursoSelecionado = idNivelCursoSelecionado;
	}
}
