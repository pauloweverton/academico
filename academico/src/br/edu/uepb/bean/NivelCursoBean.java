package br.edu.uepb.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.edu.uepb.dao.NivelCursoDAO;
import br.edu.uepb.entidades.NivelCurso;

@ManagedBean
@RequestScoped
public class NivelCursoBean {

	private List<NivelCurso> niveisCurso;
	
	public NivelCursoBean() {
		this.niveisCurso = new NivelCursoDAO().lerTudo();
	}
	
	public List<NivelCurso> getNiveisCurso() {
		return niveisCurso;
	}
}
